package com.example.dbapp;

public interface AsyncResponse {
    void processFinish(String output);
}
