package com.example.dbapp;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLEncoder;

import javax.net.ssl.HttpsURLConnection;

public class background  extends AsyncTask<String,Void,String> {


    Context context;
    public AsyncResponse delegate = null;

    public background(Context context) {
        this.context = context;
    }

    @Override
    protected void onPreExecute() {


    }



    @Override
    protected void onPostExecute(String s) {
        delegate.processFinish(s);



    }

    @Override
    protected String doInBackground(String... strings) {

        String result="";
        String method=strings[0];
        String user=strings[1];
        String pass=strings[2];

        if(method.equals("login")) {

            String connstr = "http://192.168.43.157/MyApp/login.php";

            try {
                URL url = new URL(connstr);
                HttpURLConnection http = (HttpURLConnection) url.openConnection();
                http.setRequestMethod("POST");
                http.setDoInput(true);
                http.setDoOutput(true);

                OutputStream ops = http.getOutputStream();
                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(ops, "UTF-8"));
                String data = URLEncoder.encode("user", "UTF-8") + "=" + URLEncoder.encode(user, "UTF-8")
                        + "&&" + URLEncoder.encode("pass", "UTF-8") + "=" + URLEncoder.encode(pass, "UTF-8");

                writer.write(data);
                writer.flush();
                writer.close();
                ops.close();


                InputStream ips = http.getInputStream();
                BufferedReader reader = new BufferedReader(new InputStreamReader(ips, "ISO-8859-1"));
                String line = "";
                while ((line = reader.readLine()) != null) {
                    result += line;
                }

                reader.close();
                ips.close();
                http.disconnect();

                return result;


            } catch (MalformedURLException e) {
                result = e.getMessage();
            } catch (IOException e) {
                result = e.getMessage();
            }

        }
        else if(method.equals("register")) {

            String reg_url="http://192.168.43.157/MyApp/register.php";
            try {
                URL url=new URL(reg_url);
                HttpURLConnection http = (HttpURLConnection) url.openConnection();
                http.setRequestMethod("POST");
                http.setDoOutput(true);

                OutputStream ops=http.getOutputStream();
                BufferedWriter writer=new BufferedWriter(new OutputStreamWriter(ops,"UTF-8"));
                String data = URLEncoder.encode("user", "UTF-8") + "=" + URLEncoder.encode(user, "UTF-8")
                        + "&" + URLEncoder.encode("pass", "UTF-8") + "=" + URLEncoder.encode(pass, "UTF-8");

                writer.write(data);
                writer.flush();
                writer.close();
                ops.close();

                InputStream ips=http.getInputStream();
                BufferedReader reader = new BufferedReader(new InputStreamReader(ips, "ISO-8859-1"));
                String line = "";
                while ((line = reader.readLine()) != null) {
                    result += line;
                }

                ips.close();


                return result;


            } catch (MalformedURLException e) {
               result=e.getMessage();
            } catch (ProtocolException e) {
                result=e.getMessage();
            } catch (IOException e) {
                result=e.getMessage();
            }
        }
        else if(method.equals("1") || method.equals("2")) {
            String reg_url="http://192.168.43.157/MyApp/query.php";
            try {
                URL url=new URL(reg_url);
                HttpURLConnection http = (HttpURLConnection) url.openConnection();
                http.setRequestMethod("POST");
                http.setDoOutput(true);

                OutputStream ops=http.getOutputStream();
                BufferedWriter writer=new BufferedWriter(new OutputStreamWriter(ops,"UTF-8"));
                String query,type;
                if(method.equals("1")) {
                    query = "select * from user;";
                    type="select";
                }
                else {
                    query = "delete from user where user='" + user + "' and pass='" + pass + "';";
                    type="delete";
                }
                String data = URLEncoder.encode("query", "UTF-8") + "=" + URLEncoder.encode(query, "UTF-8")+"&"+
                        URLEncoder.encode("type", "UTF-8") + "=" + URLEncoder.encode(type, "UTF-8");

                writer.write(data);
                writer.flush();
                writer.close();
                ops.close();

                InputStream ips=http.getInputStream();
                BufferedReader reader = new BufferedReader(new InputStreamReader(ips, "ISO-8859-1"));
                String line = "";
                while ((line = reader.readLine()) != null) {
                    result += line;
                    result+="\n";
                }

                ips.close();


                return result;




            } catch (MalformedURLException e) {
                result=e.getMessage();
            } catch (ProtocolException e) {
                result=e.getMessage();
            } catch (IOException e) {
                result=e.getMessage();
            }
        }


        return result;
    }
}

