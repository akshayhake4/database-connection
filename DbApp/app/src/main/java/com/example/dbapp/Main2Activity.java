package com.example.dbapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class Main2Activity extends AppCompatActivity implements AsyncResponse {

    Button del;
    Intent intent;
    background bg;
    String user,pass;
    TextView welcome;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);


        del=findViewById(R.id.btndelete);
        welcome=findViewById(R.id.welcome);
        intent=getIntent();
        user=intent.getStringExtra("user");
        pass=intent.getStringExtra("pass");

        welcome.setText("Welcome "+user);

        Toast.makeText(this,"welcome "+user,Toast.LENGTH_LONG).show();

        bg=new background(Main2Activity.this);


        //this to set delegate/listener back to this class
        bg.delegate = this;

        del.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                //execute the async task
                bg.execute("2",user,pass);

            }
        });

    }

    @Override
    public void processFinish(String output) {
        Toast.makeText(this,"DELETED SUCCESSFULLY",Toast.LENGTH_LONG).show();
        finish();
    }
}
