package com.example.dbapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class Fetch extends AppCompatActivity implements AsyncResponse {

    private TextView t;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fetch);

        t=findViewById(R.id.text);


        background bg=new background(Fetch.this);


        //this to set delegate/listener back to this class
        bg.delegate = this;

        //execute the async task
        bg.execute("1","2","3");
    }

    @Override
    public void processFinish(String output) {

        t.setText(output);

    }
}
