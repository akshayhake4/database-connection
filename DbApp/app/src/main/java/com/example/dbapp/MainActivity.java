package com.example.dbapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements AsyncResponse {

    EditText usr,pas;
    Button login,register,fetch;
    String user,pass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        usr=findViewById(R.id.username);
        pas=findViewById(R.id.password);
        login=findViewById(R.id.btnlogin);
        register=findViewById(R.id.btnregister);
        fetch=findViewById(R.id.btnfetch);


        user=usr.getText().toString();
        pass=pas.getText().toString();


        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                user=usr.getText().toString();
                pass=pas.getText().toString();
                background bg=new background(MainActivity.this);
                bg.delegate=MainActivity.this;
                bg.execute("login",user,pass);


            }
        });

        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                user=usr.getText().toString();
                pass=pas.getText().toString();
                background bg=new background(MainActivity.this);
                bg.delegate=MainActivity.this;
                bg.execute("register",user,pass);

            }
        });

        fetch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this,Fetch.class));
            }
        });
    }

    @Override
    public void processFinish(String output) {

        if(!output.equals("error")) {

            user=usr.getText().toString();
            pass=pas.getText().toString();
            Toast.makeText(this,"Successful",Toast.LENGTH_LONG).show();
            Intent intent=new Intent(MainActivity.this,Main2Activity.class);
            intent.putExtra("user",user);
            intent.putExtra("pass",pass);
            startActivity(intent);
        }
        else
        {
            Toast.makeText(this,"ERROR",Toast.LENGTH_LONG).show();
        }

    }
}
